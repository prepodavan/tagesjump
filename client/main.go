package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"strconv"
	"tjcloud/delivery/grpc/cloud_grpc"
)

const mediaDir = "./media"

func main() {
	host := flag.String("host", "localhost", "host of grpc server")
	flag.Parse()
	connectionString := fmt.Sprintf("%s:%s", *host, os.Getenv("PORT"))
	log.Printf("connecting to %s\n", connectionString)
	conn, err := grpc.Dial(connectionString, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("failed to create client conn: %s", err.Error())
	}

	defer conn.Close()

	client := cloud_grpc.NewCloudClient(conn)
	if os.Getenv("SAVE") == "1" {
		printSave(client)
	}

	if len(os.Getenv("LIST")) > 0 {
		numOfRequests, err := strconv.Atoi("-42")
		if err == nil {
			printAllLists(client, numOfRequests)
		}
	}
}

func printSave(client cloud_grpc.CloudClient) {
	wg := sync.WaitGroup{}
	files := read()
	wg.Add(len(files))
	for _, f := range files {
		go func(file *cloud_grpc.File) {
			info, err := client.Save(context.Background(), file)
			if err != nil {
				log.Printf("failed to save file; name: %s, err: %s\n", file.Name, err.Error())
			}

			bytes, _ := json.Marshal(info)
			log.Printf("name: %s; meta info: %s\n", file.Name, bytes)
			wg.Done()
		}(f)
	}

	wg.Wait()
}

func printAllLists(client cloud_grpc.CloudClient, amount int) {
	wg := sync.WaitGroup{}
	wg.Add(amount)
	for i := 0; i < amount; i++ {
		go func(){
			printList(client)
			wg.Done()
		}()
	}
	wg.Wait()
}

func printList(client cloud_grpc.CloudClient) {
	listClient, err := client.List(context.Background(), &cloud_grpc.ListRequest{})
	if err != nil {
		log.Printf("failed to list: %s\n", err.Error())
	}

	for {
		info, err := listClient.Recv()
		if err != nil {
			log.Printf("failed to list; err: %s\n", err.Error())
			break
		}

		bytes, _ := json.Marshal(info)
		log.Printf("list: %s\n", bytes)
	}
}

func read() (result []*cloud_grpc.File) {
	files, err := ioutil.ReadDir(mediaDir)
	if err != nil {
		panic(err)
	}

	for _, file := range files {
		path := mediaDir + "/" + file.Name()
		content, err := ioutil.ReadFile(path)
		if err != nil {
			panic(err)
		}

		result = append(result, &cloud_grpc.File{
			Name:                 file.Name(),
			Content:              content,
		})
	}

	return
}