module tjcloud

go 1.13

require (
	github.com/golang-collections/collections v0.0.0-20130729185459-604e922904d3 // indirect
	github.com/golang/protobuf v1.3.2
	google.golang.org/grpc v1.25.1
)
