package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	impl "tjcloud/delivery/grpc"
	"tjcloud/repository"
	"tjcloud/usecase"
)

func main() {
	repo := repository.NewRepo()
	uc := usecase.NewUseCase(repo)

	connStr := fmt.Sprintf(":%s", os.Getenv("PORT"))
	lis, err := net.Listen("tcp", connStr)
	if err != nil {
		log.Fatalf("failed to listen: %s", err.Error())
	}

	grpcServer := grpc.NewServer()
	impl.NewCloudServer(grpcServer, uc, uc)
	log.Println("server started")
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %s", err.Error())
	}
}