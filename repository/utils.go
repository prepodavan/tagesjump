package repository

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
	"tjcloud/models"
)

func (r *Repository) buildName(f *models.FileBaseMetaInfo, created, updated time.Time) string {
	return fmt.Sprintf("%s/%s.%d.%d", mediaDir, f.Name, created.Unix(), updated.Unix())
}

func (r *Repository) isExists(meta *models.FileBaseMetaInfo) (isExists bool, filename string, err error) {
	var files []os.FileInfo
	files, err = ioutil.ReadDir(mediaDir)
	if err != nil {
		return false, "", err
	}

	for _, file := range files {
		if strings.HasPrefix(file.Name(), meta.Name) {
			return true, file.Name(), nil
		}
	}

	return
}

func (r *Repository) parseName(name string) *models.FileMetaInfo {
	startOfUpdated := strings.LastIndex(name, ".")
	startOfCreated := strings.LastIndex(name[:startOfUpdated], ".")
	createdUnix, _ := strconv.Atoi(name[startOfCreated+1:startOfUpdated])
	updatedUnix, _ := strconv.Atoi(name[startOfUpdated+1:])
	created := time.Unix(int64(createdUnix), 0)
	updated := time.Unix(int64(updatedUnix), 0)
	return &models.FileMetaInfo{
		FileBaseMetaInfo: models.FileBaseMetaInfo{
			Name: name[:startOfCreated],
		},
		CreatedAt: &created,
		UpdatedAt: &updated,
	}
}

func (r *Repository) removeOldIfExists(fn string) error {
	if _, err := os.Stat(fn); os.IsNotExist(err) {
		return nil
	} else if err != nil {
		return err
	}

	if err := os.Remove(fn); err != nil {
		return err
	}

	return nil
}