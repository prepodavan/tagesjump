package repository

import (
	"io/ioutil"
	"os"
	"time"
	"tjcloud/models"
)

func (r *Repository) create(input *models.FileInput) (*models.FileMetaInfo, error) {
	now := time.Now()
	name := r.buildName(&input.FileBaseMetaInfo, now, now)
	if err := ioutil.WriteFile(name, input.Content, os.ModePerm); err != nil {
		return nil, err
	}

	return &models.FileMetaInfo{
		FileBaseMetaInfo: input.FileBaseMetaInfo,
		CreatedAt: &now,
		UpdatedAt: &now,
	}, nil
}