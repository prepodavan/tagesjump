package repository

import (
	"io/ioutil"
	"tjcloud/models"
)

func (r *Repository) List() (fileList []*models.FileMetaInfo, err error) {
	files, err := ioutil.ReadDir(mediaDir)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		if file.Name()[0] == '.' {
			continue
		}

		fileList = append(fileList, r.parseName(file.Name()))
	}

	return
}