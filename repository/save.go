package repository

import (
	"tjcloud/models"
)

func (r *Repository) Save(input *models.FileInput) (info *models.FileMetaInfo, err error) {
	if isExists, name, err := r.isExists(&input.FileBaseMetaInfo); err != nil {
		return nil, err
	} else if !isExists {
		return r.create(input)
	} else {
		return r.update(input, name)
	}
}