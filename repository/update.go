package repository

import (
	"io/ioutil"
	"os"
	"time"
	"tjcloud/models"
)

func (r *Repository) update(input *models.FileInput, filename string) (*models.FileMetaInfo, error) {
	now := time.Now()
	info := r.parseName(filename)
	if err := ioutil.WriteFile(filename, input.Content, os.ModePerm); err != nil {
		return nil, err
	}

	newName := r.buildName(&info.FileBaseMetaInfo, *info.CreatedAt, now)
	if err := os.Rename(filename, newName); err != nil {
		return nil, err
	}

	if err := r.removeOldIfExists(mediaDir + "/" + filename); err != nil {
		return nil, err
	}

	return &models.FileMetaInfo{
		FileBaseMetaInfo: input.FileBaseMetaInfo,
		CreatedAt: info.CreatedAt,
		UpdatedAt: &now,
	}, nil
}