package repository

import (
	"crypto/md5"
	"hash"
)

const mediaDir = "./media"

type Repository struct {
	hasher hash.Hash
}

func NewRepo() *Repository {
	return &Repository{
		hasher: md5.New(),
	}
}