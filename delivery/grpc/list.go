package grpc

import (
	"log"
	"sync/atomic"
	cloud "tjcloud/delivery/grpc/cloud_grpc"
)

const listPoolMaxSize = 100

var listings          int32


func (s *server) List(req *cloud.ListRequest, stream cloud.Cloud_ListServer) error {
	if listings == listPoolMaxSize {
		return PoolSizeErr
	}

	atomic.AddInt32(&listings, 1)
	defer atomic.AddInt32(&listings, -1)

	list, err := s.lister.List()
	if err != nil {
		log.Println(err.Error())
		return err
	}

	for _, modelInfo := range list {
		grpcInfo, err := s.buildInfo(modelInfo)
		if err != nil {
			log.Println(err.Error())
			return err
		}

		if err = stream.Send(grpcInfo); err != nil {
			log.Println(err.Error())
			return err
		}
	}

	return nil
}