package grpc

import (
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	cloud "tjcloud/delivery/grpc/cloud_grpc"
	"tjcloud/models"
)

func (s *server) buildInput(f *cloud.File) *models.FileInput {
	return &models.FileInput{
		FileBaseMetaInfo: models.FileBaseMetaInfo{
			Name:f.Name,
		},
		Content:          f.Content,
	}
}

func (s *server) buildInfo(f *models.FileMetaInfo) (_ *cloud.FileMetaInfo, err error) {
	var created, updated *timestamp.Timestamp
	if f.CreatedAt != nil {
		created, err = ptypes.TimestampProto(*f.CreatedAt)
		if err != nil {
			return nil, err
		}
	}

	if f.UpdatedAt != nil {
		updated, err = ptypes.TimestampProto(*f.UpdatedAt)
		if err != nil {
			return nil, err
		}
	}

	return &cloud.FileMetaInfo{
		Name: f.Name,
		Created:        created,
		Updated:        updated,
	}, nil
}