package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	cloud "tjcloud/delivery/grpc/cloud_grpc"
	"tjcloud/models"
)

var (
	PoolSizeErr       = status.Error(1, "max connection pool size reached")
	ResourceLockedErr = status.Error(2, "resource locked")
)


type Saver interface {
	Save(*models.FileInput) (*models.FileMetaInfo, error)
}

type Lister interface {
	List() (fileList []*models.FileMetaInfo, err error)
}

func NewCloudServer(gserver *grpc.Server, saver Saver, lister Lister) {
	srv := &server{
		saver:saver,
		lister:lister,
	}

	cloud.RegisterCloudServer(gserver, srv)
	reflection.Register(gserver)
}

type server struct {
	saver Saver
	lister Lister
}