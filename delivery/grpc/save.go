package grpc

import (
	"context"
	"log"
	"sync"
	cloud "tjcloud/delivery/grpc/cloud_grpc"
)

const savePoolMaxSize = 10

var saves = sync.Map{}

func SavesLength() int {
	i := 0
	saves.Range(func(_, _ interface{}) bool {
		i++
		return true
	})

	return i
}

func (s *server) Save(ctx context.Context, f *cloud.File) (*cloud.FileMetaInfo, error) {
	if SavesLength() == savePoolMaxSize {
		return nil, PoolSizeErr
	}

	if _, ok := saves.Load(f.Name); ok {
		return nil, ResourceLockedErr
	}

	saves.Store(f.Name, struct{}{})
	defer saves.Delete(f.Name)

	modelInfo, err := s.saver.Save(s.buildInput(f))
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	grpcInfo, err := s.buildInfo(modelInfo)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return grpcInfo, nil
}
