package usecase

import "tjcloud/models"

type IRepo interface {
	Save(input *models.FileInput) (info *models.FileMetaInfo, err error)
	List() (fileList []*models.FileMetaInfo, err error)
}

type UseCase struct {
	repo IRepo
}

func NewUseCase(repo IRepo) *UseCase {
	return &UseCase{repo:repo}
}

func (uc *UseCase) Save(input *models.FileInput) (info *models.FileMetaInfo, err error) {
	return uc.repo.Save(input)
}

func (uc *UseCase) List() (fileList []*models.FileMetaInfo, err error) {
	return uc.repo.List()
}