package models

import (
	"time"
)

type FileBaseMetaInfo struct {
	Name string
}

type FileInput struct {
	FileBaseMetaInfo
	Content []byte
}

type FileMetaInfo struct {
	FileBaseMetaInfo
	CreatedAt              *time.Time
	UpdatedAt              *time.Time
}