FROM golang:alpine

ENV GO111MODULE=on
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN CGO_ENABLED=0 go build -o main
ENTRYPOINT ["/app/main"]